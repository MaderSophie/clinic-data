package com.example.clinicdata

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class Medication: AppCompatActivity() {
    lateinit var newLongtermMedicationButton: AppCompatButton
    lateinit var newPrescribedMedicationButton: AppCompatButton
    lateinit var deleteButton: Button
    /*val dataList = arrayListOf<String>()
    lateinit var adapter: ArrayAdapter<String>*/
    lateinit var listView: RecyclerView

    private lateinit var dbref : DatabaseReference
    private lateinit var medicationLongtermRecyclerView : RecyclerView
    private lateinit var medicationLongtermArrayList : ArrayList<LongtermMedicationData>

    private lateinit var medicationPrescribeRecyclerView : RecyclerView
    private lateinit var medicationPrescribeArrayList : ArrayList<PrescribeMedicationData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medication)

        medicationLongtermRecyclerView = findViewById(R.id.listLongtermMedication)
        medicationLongtermRecyclerView.layoutManager = LinearLayoutManager(this)
        medicationLongtermRecyclerView.setHasFixedSize(true)

        medicationLongtermArrayList = arrayListOf<LongtermMedicationData>()
        getLongtermMedicationInformation()


        medicationPrescribeRecyclerView = findViewById(R.id.listPrescribedMedication)
        medicationPrescribeRecyclerView.layoutManager = LinearLayoutManager(this)
        medicationPrescribeRecyclerView.setHasFixedSize(true)

        medicationPrescribeArrayList = arrayListOf<PrescribeMedicationData>()
        getPrescribeMedicationInformation()

        //Neues Dauermedikament eintragen
        newLongtermMedicationButton = findViewById(R.id.newLongtermMedicationButton)
        newLongtermMedicationButton.setOnClickListener {
            val intent = Intent(this@Medication, MedicationNewLongterm::class.java)
            startActivity(intent)
        }

        //Neues verschriebenes Medikament eintragen
        newPrescribedMedicationButton = findViewById(R.id.newPrescribedMedicationButton)
        newPrescribedMedicationButton.setOnClickListener {
            val intent = Intent(this@Medication, MedicationNewPrescribe::class.java)
            startActivity(intent)
        }

        //Navbar
        val notesButtonNavbar = findViewById<Button>(R.id.notesButtonNavbar)
        notesButtonNavbar.setOnClickListener {
           val intent = Intent(this@Medication, Notes::class.java)
            startActivity(intent)
        }

        val appointmentButtonNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentButtonNavbar.setOnClickListener {
            val intent = Intent(this@Medication, Appointments::class.java)
            startActivity(intent)
        }

        val diagnosticFindingsButtonNavbar = findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        diagnosticFindingsButtonNavbar.setOnClickListener {
            val intent = Intent(this@Medication, DiagnosticFindings::class.java)
            startActivity(intent)
        }

        val medicalRoundButtonNavbar = findViewById<Button>(R.id.medicalRoundButtonNavbar)
        medicalRoundButtonNavbar.setOnClickListener {
            val intent = Intent(this@Medication, MedicalRound::class.java)
            startActivity(intent)
        }


            //Button löschen
        listView = findViewById(R.id.listLongtermMedication)
        deleteButton = findViewById(R.id.deleteLongtermMedicationButton)

        /*adapter = ArrayAdapter(this, android.R.layout.simple_list_item_single_choice, dataList)
        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val item = dataList[position]
            deleteButton.setOnClickListener {
                dataList.remove(item)
                adapter.notifyDataSetChanged()
            }
        }

        */
        /*
        //Daten aus der Firebase abrufen
        val database = FirebaseDatabase.getInstance()
        val medicationRef = database.getReference("medicationNewLongterm")

        medicationRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dataList.clear()
                for (medicationSnapshot in dataSnapshot.children) {
                    val medication = medicationSnapshot.getValue(String::class.java)
                    if (medication != null) {
                        dataList.add(medication)
                    }
                }
                adapter.notifyDataSetChanged()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Handle Database Error
            }
        })

        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val item = dataList[position]
            deleteButton.setOnClickListener {
                medicationRef.child(item).removeValue()
                dataList.remove(item)
                adapter.notifyDataSetChanged()
            }
        }*/


    }

    private fun getLongtermMedicationInformation(){
        dbref = FirebaseDatabase.getInstance().getReference("medicationNewLongterm/longtermMedications")

        dbref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists())
                {
                    for(longtermMedicationSnapshot in snapshot.children){
                        val longtermMedication = longtermMedicationSnapshot.getValue(LongtermMedicationData::class.java)
                        medicationLongtermArrayList.add(longtermMedication!!)
                    }

                    medicationLongtermRecyclerView.adapter = MyAdapterMedication(medicationLongtermArrayList)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }


        })
    }


    private fun getPrescribeMedicationInformation(){
        dbref = FirebaseDatabase.getInstance().getReference("medicationNewPrescribe/prescribeMedications")

        dbref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists())
                {
                    for(prescribeMedicationSnapshot in snapshot.children){
                        val prescribeMedication = prescribeMedicationSnapshot.getValue(PrescribeMedicationData::class.java)
                        medicationPrescribeArrayList.add(prescribeMedication!!)
                    }

                    medicationPrescribeRecyclerView.adapter = MyAdapterPrescribeMedication(medicationPrescribeArrayList)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }


        })
    }
}
