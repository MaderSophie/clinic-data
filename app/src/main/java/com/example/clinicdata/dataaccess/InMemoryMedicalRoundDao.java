/*package com.example.clinicdata.dataaccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryMedicalRoundDao implements MedicalRoundDao{
    private Map<Integer, MedicalRound> database;

    public InMemoryMedicalRoundDao() {
        this.database = new HashMap<>();
    }

    @Override
    public List<MedicalRound> loadAllMedicalRounds() {
        return new ArrayList<>(this.database.values());
    }

    @Override
    public MedicalRound loadMedicalRound(Integer id) {
        return this.database.get(id);
    }

    @Override
    public void save(MedicalRound md) {
        this.database.put(md.getId(), md);
    }

    @Override
    public void delete(Integer id) {
        this.database.remove(id);
    }
}*/
