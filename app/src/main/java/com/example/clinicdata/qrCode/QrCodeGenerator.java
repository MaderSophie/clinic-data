package com.example.clinicdata.qrCode;

import android.os.Build;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.nio.file.Paths;

public class QrCodeGenerator {
    public static void main(String[] args) throws Exception {

        String data = "https://www.hakwt.at";
        String path = "D:\\QR-Code\\infybuzz.jpg";

        BitMatrix matrix = new MultiFormatWriter()
                .encode(data, BarcodeFormat.QR_CODE, 500, 500);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            MatrixToImageWriter.writeToPath(matrix, "jpg", Paths.get(path));
        }
    }
}