package com.example.clinicdata

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
//import com.example.clinicdata.database.DatabaseHelper

class NewPatient: AppCompatActivity() {

    //private lateinit var dbHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.newpatient)

       // dbHelper = DatabaseHelper(this)

        //navabar
        val medicationNavbar = findViewById<Button>(R.id.medicationButtonNavbar)
        medicationNavbar.setOnClickListener {
            val intent = Intent(this@NewPatient, Medication::class.java)

            startActivity(intent)
        }

        val notesNavbar = findViewById<Button>(R.id.notesButtonNavbar)
        notesNavbar.setOnClickListener {
            val intent = Intent(this@NewPatient, Notes::class.java)

            startActivity(intent)
        }

        val appointmentsNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentsNavbar.setOnClickListener {
            val intent = Intent(this@NewPatient, Appointments::class.java)

            startActivity(intent)
        }

        val diagnosticfindingsNavbar = findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        diagnosticfindingsNavbar.setOnClickListener {
            val intent = Intent(this@NewPatient, DiagnosticFindings::class.java)

            startActivity(intent)
        }

        val medicalRoundNavbar = findViewById<Button>(R.id.medicalRoundButtonNavbar)
        medicalRoundNavbar.setOnClickListener {
            val intent = Intent(this@NewPatient, MedicalRound::class.java)

            startActivity(intent)
        }

        //Dropdown-Feld
        val spinnerInsuranceStatus: Spinner = findViewById(R.id.spinnerInsuranceStatus)
        val insuranceStatusArray = resources.getStringArray(R.array.insurance_status_array)
        val adapterInsurance = ArrayAdapter(this, android.R.layout.simple_spinner_item, insuranceStatusArray)
        adapterInsurance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerInsuranceStatus.adapter = adapterInsurance

        val spinnerGender: Spinner = findViewById(R.id.spinnerGender)
        val spinnerGenderArray = resources.getStringArray(R.array.gender_array)
        val adapterGender = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerGenderArray)
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGender.adapter = adapterGender

        /*//Speichern der Daten
        val firstNameEditText = findViewById<EditText>(R.id.editFirstname)
        val lastNameEditText = findViewById<EditText>(R.id.editSurname)
        val birthDateEditText = findViewById<EditText>(R.id.editBirthdate)
        val insuranceNameEditText = findViewById<EditText>(R.id.editInsuranceName)
        val insuranceNumberEditText = findViewById<EditText>(R.id.editInsuranceNumber)
        val diagnosisEditText = findViewById<EditText>(R.id.editDiagnosis)
        val streetEditText = findViewById<EditText>(R.id.editStreet)
        val houseNumberEditText = findViewById<EditText>(R.id.editHouseNumber)
        val postCodeEditText = findViewById<EditText>(R.id.editPostcode)
        val cityEditText = findViewById<EditText>(R.id.editCity)
        val phoneNumberEditText = findViewById<EditText>(R.id.editPhoneNumber)
        val savePatientdataButton = findViewById<Button>(R.id.savePatientButton)

        savePatientdataButton.setOnClickListener {
            val insuranceStatus = spinnerInsuranceStatus.selectedItem.toString()
            val gender = spinnerGender.selectedItem.toString()
            val firstName = firstNameEditText.text.toString()
            val surname = lastNameEditText.text.toString()
            val birthDate = birthDateEditText.text.toString()
            val insuranceName = insuranceNameEditText.text.toString()
            val insuranceNumber = insuranceNumberEditText.text.toString()
            val diagnosis = diagnosisEditText.text.toString()
            val street = streetEditText.text.toString()
            val houseNumber = houseNumberEditText.text.toString()
            val postCode = postCodeEditText.text.toString()
            val city = cityEditText.text.toString()
            val phoneNumber = phoneNumberEditText.text.toString()*/

           /* if (firstName.isNotEmpty() && surname.isNotEmpty() && gender.isNotEmpty() &&
                birthDate.isNotEmpty() && insuranceName.isNotEmpty() && insuranceNumber.isNotEmpty() &&
                insuranceStatus.isNotEmpty() && diagnosis.isNotEmpty() && street.isNotEmpty() &&
                houseNumber.isNotEmpty() && postCode.isNotEmpty() && city.isNotEmpty() &&
                phoneNumber.isNotEmpty()
            ) {
                dbHelper.savePatient(
                    firstName,
                    surname,
                    gender,
                    birthDate,
                    insuranceName,
                    insuranceNumber,
                    insuranceStatus,
                    diagnosis,
                    street,
                    houseNumber,
                    postCode,
                    city,
                    phoneNumber)

                //spinnerInsuranceStatus.setText(insuranceStatus)
               // spinnerGender.setText(insuranceStatus)

                } else {
                Log.e("NewPatient", "Alle Felder müssen ausgefüllt sein!")
                }*/
        }
    }
