package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

/*import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.squareup.picasso.Picasso

class ImageAdapter(private val imageList: ArrayList<ImageData>) : RecyclerView.Adapter<ImageAdapter.MyViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.diagnostic_findings_item,parent,false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ImageAdapter.MyViewHolder, position: Int) {
        val currentItem = imageList[position]

        Picasso.get().load(currentItem.imgUri).into(holder.imgUri)
    }

    class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)
    {
        val imgUri : ImageView = itemView.findViewById(R.id.firebaseImage)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}*/

class ImageAdapter(private val imageList: ArrayList<ImageData>) : RecyclerView.Adapter<ImageAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.diagnostic_findings_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = imageList[position]

        Picasso.get().load(currentItem.imgUri).into(holder.imgUri)

        // Wenn Sie auch den Text anzeigen möchten, können Sie dies hier hinzufügen:
        // holder.imgTitle.text = currentItem.imgTitle
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgUri: ImageView = itemView.findViewById(R.id.firebaseImage)
        // Wenn Sie auch den Text anzeigen möchten, fügen Sie dies hier hinzu:
        // val imgTitle: TextView = itemView.findViewById(R.id.firebaseImageTitle)
    }

    override fun getItemCount(): Int {
        return imageList.size
    }
}