package com.example.clinicdata

data class LongtermMedicationData( var name : String ?= null, var dosis : String ?= null, var endTime : String ?= null, var frequency : String ?= null, var prescriber : String ?= null, var startTime : String ?= null)
