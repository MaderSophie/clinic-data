package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapterMedication(private  val medicationList : ArrayList<LongtermMedicationData>) : RecyclerView.Adapter<MyAdapterMedication.MyViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.medication_longterm_item,
        parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyAdapterMedication.MyViewHolder, position: Int) {
        val currentitem = medicationList[position]

        holder.name.text = currentitem.name
        holder.dosis.text = currentitem.dosis
        holder.endTime.text = currentitem.endTime
        holder.frequency.text = currentitem.frequency
        holder.prescriber.text = currentitem.prescriber
        holder.startTime.text = currentitem.startTime
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val name : TextView = itemView.findViewById(R.id.databaseMedication)
        val dosis : TextView = itemView.findViewById(R.id.databaseDosis)
        val endTime : TextView = itemView.findViewById(R.id.databaseEndTime)
        val frequency : TextView = itemView.findViewById(R.id.databaseFrequency)
        val prescriber : TextView = itemView.findViewById(R.id.databasePrescriber)
        val startTime : TextView = itemView.findViewById(R.id.databaseStartTime)
    }

    override fun getItemCount(): Int {
        return medicationList.size
    }

}