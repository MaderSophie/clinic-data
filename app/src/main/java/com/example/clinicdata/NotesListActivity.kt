package com.example.clinicdata

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class NotesListActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var adapter: ArrayAdapter<String>
    private val notesList = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notes)
        println("Hier bin ich 5")
        listView = findViewById(R.id.listNotes)
        println("Hier bin ich 6")
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, notesList)
        println("Hier bin ich 7")
        listView.adapter = adapter
        println("Hier bin ich 8")

        val database = FirebaseDatabase.getInstance()
        println("Hier bin ich 9")
        val notesRef = database.getReference("Notes/newNotes")
        println("Hier bin ich 10")

        notesRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot)
            {
                runOnUiThread {
                    notesList.clear()
                    println("Hier bin ich 11")
                    for (noteSnapshot in dataSnapshot.children) {
                        val note = noteSnapshot.value as? HashMap<*, *>
                        if (note != null) {
                            val noteTitle = note["title"] as? String
                            println("Hier bin ich 12")
                            if (noteTitle != null) {
                                notesList.add(noteTitle)
                            }
                        }
                    }
                    adapter.notifyDataSetChanged()
                    println("Hier bin ich 13")
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Log.e("NotesListActivity", "Faild to read value", databaseError.toException())
            }
        })
    }
}