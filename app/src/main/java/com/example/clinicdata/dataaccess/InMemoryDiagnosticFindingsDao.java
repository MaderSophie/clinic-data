/*package com.example.clinicdata.dataaccess;

import com.example.clinicdata.DiagnosticFindings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryDiagnosticFindingsDao implements DiagnosticFindingsDao{
    private Map<Integer, DiagnosticFindings> database;

    public InMemoryDiagnosticFindingsDao() {
        this.database = new HashMap<>();
    }
    @Override
    public List<DiagnosticFindings> loadAllDiagnosticFindings() {
        return new ArrayList<>(this.database.values());
    }

    @Override
    public DiagnosticFindings loadDiagnosticFindings(Integer id) {
        return this.database.get(id);
    }

    @Override
    public void save(DiagnosticFindings df) {
        this.database.put(df.getId(),df);
    }

    @Override
    public void delete(Integer id) {
        this.database.remove(id);
    }
}*/
