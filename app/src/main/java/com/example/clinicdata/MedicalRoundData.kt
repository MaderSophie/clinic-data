package com.example.clinicdata

data class MedicalRoundData(var doctor : String ?= null, var medicalRound : String ?= null, var symptoms : String ?= null, var anamnesis : String ?= null)
