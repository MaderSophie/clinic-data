package com.example.clinicdata

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import org.springframework.context.i18n.LocaleContextHolder.setLocale
import java.util.Locale

class Appointments: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.appointments)

        setLocale("de")
        val newEntryButton = findViewById<Button>(R.id.newEntryButtonAppointments)
        val editEntryButton = findViewById<Button>(R.id.editAppointmentsButton)
        val deleteEntryButton = findViewById<Button>(R.id.deleteAppointmentsButton)

        val medicationNavbar = findViewById<Button>(R.id.medicationButtonNavbar)
        medicationNavbar.setOnClickListener {
            val intent = Intent(this@Appointments, Medication::class.java)

            startActivity(intent)
        }

        //Navbar
        val notesNavbar = findViewById<Button>(R.id.notesButtonNavbar)
        notesNavbar.setOnClickListener {
            val intent = Intent(this@Appointments, Notes::class.java)

            startActivity(intent)
        }

        val appointmentsNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentsNavbar.setOnClickListener {
            val intent = Intent(this@Appointments, Appointments::class.java)

            startActivity(intent)
        }

        val diagnosticfindingsNavbar = findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        diagnosticfindingsNavbar.setOnClickListener {
            val intent = Intent(this@Appointments, DiagnosticFindings::class.java)

            startActivity(intent)
        }

        val medicalRoundNavbar = findViewById<Button>(R.id.medicalRoundButtonNavbar)
        medicalRoundNavbar.setOnClickListener {
            val intent = Intent(this@Appointments, MedicalRound::class.java)

            startActivity(intent)
        }

        //Buttons
        newEntryButton.setOnClickListener {
            val intent = Intent(this@Appointments, NewPatient::class.java)

            startActivity(intent)
        }

        editEntryButton.setOnClickListener {
            val intent = Intent(this@Appointments, NewPatient::class.java)

            startActivity(intent)
        }

        deleteEntryButton.setOnClickListener {
            val intent = Intent(this@Appointments, NewPatient::class.java)

            startActivity(intent)
        }
    }

    fun setLocale(languageCode: String) {
        val locale = Locale(languageCode)
        Locale.setDefault(locale)

        val config = Configuration()
        config.locale = locale

        resources.updateConfiguration(config, resources.displayMetrics)
    }
}