package com.example.clinicdata
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class DiagnosticFindings : AppCompatActivity()
{

    //private lateinit var storageRef: StorageReference
    //private lateinit var imageAdapter : ImageAdapter

    private val imageList = ArrayList<ImageData>()
    private lateinit var storageReference: StorageReference
    private lateinit var diagnosticFindingsRecyclerView : RecyclerView


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.diagnostic_findings)



        storageReference = FirebaseStorage.getInstance().reference


        diagnosticFindingsRecyclerView = findViewById(R.id.listDiagnosticFindings)
        diagnosticFindingsRecyclerView.layoutManager = LinearLayoutManager(this)
        diagnosticFindingsRecyclerView.setHasFixedSize(true)

        val imagesRef = storageReference.child("images")
        imagesRef.listAll().addOnSuccessListener { result ->
            for (imageMetadata in result.items) {
                // Download the image file and add it to the image list
                val imageRef = storageReference.child("images/${imageMetadata.name}")
                imageRef.downloadUrl.addOnSuccessListener { imageUri ->
                    imageList.add(ImageData(imageUri.toString()))
                    updateRecyclerView()
                }.addOnFailureListener { exception ->
                    // Handle the error
                }
            }
        }.addOnFailureListener { exception ->
            // Handle the error
        }


        val button: Button = findViewById(R.id.uploadButtonDiagnosticFindings)
        button.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent,1)
        }


        val medicationButonNavbar = findViewById<Button>(R.id.medicationButtonNavbar)
        medicationButonNavbar.setOnClickListener {
            val intent = Intent(this@DiagnosticFindings, Medication::class.java)
            startActivity(intent)
        }

        val notesButtonNavbar = findViewById<Button>(R.id.notesButtonNavbar)
        notesButtonNavbar.setOnClickListener {
            val intent = Intent(this@DiagnosticFindings, Notes::class.java)
            startActivity(intent)
        }

        val appointmentButtonNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentButtonNavbar.setOnClickListener {
            val intent = Intent(this@DiagnosticFindings, Appointments::class.java)
            startActivity(intent)
        }

        val medicalRoundButtonNavbar = findViewById<Button>(R.id.medicalRoundButtonNavbar)
        medicalRoundButtonNavbar.setOnClickListener {
            val intent = Intent(this@DiagnosticFindings, MedicalRound::class.java)
            startActivity(intent)
        }


    }


    private fun updateRecyclerView() {


        // Initialize the RecyclerView
        val layoutManager = LinearLayoutManager(this)
        diagnosticFindingsRecyclerView.layoutManager = layoutManager

        // Initialize the ImageAdapter and set it to the RecyclerView
        val imageAdapter = ImageAdapter(imageList)
        diagnosticFindingsRecyclerView.adapter = imageAdapter


        /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {

        }*/
    }


}