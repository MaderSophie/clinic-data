package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PatientAdapter(private val personList: List<Patient>) : RecyclerView.Adapter<PatientAdapter.PersonViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.patientlist, parent, false)
        return PersonViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val currentPerson = personList[position]

        holder.firstNameTextView.text = currentPerson.firstname
        holder.lastNameTextView.text = currentPerson.surname
        holder.birthDateTextView.text = currentPerson.birthdate.toString()
    }

    override fun getItemCount() = personList.size

    class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstNameTextView: TextView = itemView.findViewById(R.id.editFirstname)
        val lastNameTextView: TextView = itemView.findViewById(R.id.editSurname)
        val birthDateTextView: TextView = itemView.findViewById(R.id.editBirthdate)
    }
}