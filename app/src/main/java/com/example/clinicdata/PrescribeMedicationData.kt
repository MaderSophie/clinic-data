package com.example.clinicdata

data class PrescribeMedicationData(var name : String ?= null, var dosisPrescribe : String ?= null, var endTimePrescribe: String ?= null, var frequencyPrescribe : String ?= null, var prescriberPrescribe : String ?= null, var startTimePrescribe : String ?= null)
