package com.example.clinicdata

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.Locale
import java.text.SimpleDateFormat
import  java.util.*


class MedicalRoundNew : AppCompatActivity() {

  private lateinit var database: FirebaseDatabase
  private lateinit var myRef: DatabaseReference

    lateinit var checkBox1: CheckBox
    lateinit var  checkBox2: CheckBox

    lateinit var button: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medical_round_new)

        val dateTextView = findViewById<TextView>(R.id.editDateText)
        val date = Date()
        val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

        dateTextView.text = format.format(date)

        val saveMedicalRoundButton = findViewById<Button>(R.id.saveMedicalRoundButton)
        val medicalRoundDate = findViewById<EditText>(R.id.editDateText)
        val doctor = findViewById<EditText>(R.id.editDoctorText)
        val symptoms = findViewById<EditText>(R.id.editSymptomsText)
        val anamnesis = findViewById<EditText>(R.id.EditAnamnesisText)
        val checkboxYes = findViewById<CheckBox>(R.id.checkboxYes)
        val checkboxNo = findViewById<CheckBox>(R.id.checkboxNo)

        saveMedicalRoundButton.setOnClickListener{
            if (validateInputs(medicalRoundDate, doctor, symptoms, anamnesis, checkboxYes, checkboxNo)){
                saveMedicalRoundData(
                    medicalRoundDate.text.toString(),
                    doctor.text.toString(),
                    symptoms.text.toString(),
                    anamnesis.text.toString(),
                    checkboxYes.isChecked,
                    checkboxNo.isChecked
                )
                medicalRoundDate.setText("")
                doctor.setText("")
                symptoms.setText("")
                anamnesis.setText("")
                checkboxYes.setText("")
                checkboxNo.setText("")
                finish()
            }else {
                Toast.makeText(this, "Bitte füllen Sie alle erforderlichen Felder aus", Toast.LENGTH_SHORT).show()
            }
        }

        //nur eine Checkbox auswählen können

        checkBox1 = findViewById(R.id.checkboxYes)
        checkBox2 = findViewById(R.id.checkboxNo)

        checkBox1.setOnClickListener{
            checkBox2.isChecked = false
        }
        checkBox2.setOnClickListener{
            checkBox1.isChecked = false
        }

        //Button der zu Medikamente verlinkt
        button = findViewById(R.id.goToMedicationButton)

        button.setOnClickListener{
            val intent = Intent(this@MedicalRoundNew, Medication::class.java)
            startActivity(intent)
        }
    }

    private fun validateInputs(
        medicalRound: EditText,
        doctor: EditText,
        symptoms: EditText,
        anamnesis: EditText,
        checkboxYes: CheckBox,
        checkboxNo: CheckBox
        ): Boolean{
        return !(TextUtils.isEmpty(medicalRound.text) &&
                TextUtils.isEmpty(doctor.text) &&
                TextUtils.isEmpty(symptoms.text) &&
                TextUtils.isEmpty(anamnesis.text) &&
                !checkboxYes.isChecked &&
                !checkboxNo.isChecked)
    }

    private fun saveMedicalRoundData(
        medicalRound: String,
        doctor: String,
        symptoms: String,
        anamnesis: String,
        checkboxYes: Boolean,
        checkboxNo: Boolean
    ){
        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("newMedicalRound")

        val medicalRoundData = hashMapOf(
            "medicalRound" to medicalRound,
            "doctor" to doctor,
            "symptoms" to symptoms,
            "anamnesis" to anamnesis,
            "checkboxYes" to checkboxYes,
            "checkboxNo" to checkboxNo
        )

        myRef.child("medicalRound").push().setValue(medicalRoundData)
            .addOnSuccessListener {
                Toast.makeText(this, "Medical Round data saved successfully", Toast.LENGTH_SHORT).show()
            }
    }
}