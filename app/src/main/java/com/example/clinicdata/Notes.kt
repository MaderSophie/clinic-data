package com.example.clinicdata

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class Notes: AppCompatActivity() {

    lateinit var button: AppCompatButton

    private lateinit var dbref : DatabaseReference
    private lateinit var notesRecyclerView : RecyclerView
    private lateinit var notesArrayList : ArrayList<NotesData>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notes)

        notesRecyclerView = findViewById(R.id.listNotes)
        notesRecyclerView.layoutManager = LinearLayoutManager(this)
        notesRecyclerView.setHasFixedSize(true)

        notesArrayList = arrayListOf<NotesData>()
        getNotesInformation()

        button = findViewById(R.id.newEntryNotesButton)
        button.setOnClickListener {
            val intent = Intent(this@Notes, NewNotes::class.java)
            startActivity(intent)
        }


        //Navbar
        val medicationButtonNavbar = findViewById<Button>(R.id.medicationButtonNavbar)
        medicationButtonNavbar.setOnClickListener {
            val intent = Intent(this@Notes, Medication::class.java)
            startActivity(intent)
        }

        val appointmentButtonNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentButtonNavbar.setOnClickListener {
            val intent = Intent(this@Notes, Appointments::class.java)
            startActivity(intent)
        }

        val diagnosticFindingsButtonNavbar = findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        diagnosticFindingsButtonNavbar.setOnClickListener {
            val intent = Intent(this@Notes, DiagnosticFindings::class.java)
            startActivity(intent)
        }

        val medicalRoundButtonNavbar = findViewById<Button>(R.id.medicalRoundButtonNavbar)
        medicalRoundButtonNavbar.setOnClickListener {
            val intent = Intent(this@Notes, MedicalRound::class.java)
            startActivity(intent)
        }
    }

    private fun getNotesInformation(){
        dbref = FirebaseDatabase.getInstance().getReference("Notes/newNotes")

        dbref.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
               if (snapshot.exists()){}
                for(noteSnapshot in snapshot.children){
                   val notes = noteSnapshot.getValue(NotesData::class.java)
                    notesArrayList.add(notes!!)
                }

                notesRecyclerView.adapter = MyAdapterNotes(notesArrayList)
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }
}