package com.example.clinicdata

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MedicalRound: AppCompatActivity() {

    private lateinit var dbref : DatabaseReference
    private lateinit var medicalRoundRecyclerView : RecyclerView
    private lateinit var medicalRoundArrayList : ArrayList<MedicalRoundData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medical_round)

        val newMedicalRoundButton = findViewById<Button>(R.id.newEntryButtonMedicalRound)

        newMedicalRoundButton.text = "Neu"
        newMedicalRoundButton.setOnClickListener {
            val intent = Intent(this@MedicalRound, MedicalRoundNew::class.java)

            startActivity(intent)
        }

        medicalRoundRecyclerView = findViewById(R.id.listMedicalRound)
        medicalRoundRecyclerView.layoutManager = LinearLayoutManager(this)
        medicalRoundRecyclerView.setHasFixedSize(true)

        medicalRoundArrayList = arrayListOf<MedicalRoundData>()
        getMedicalRoundInformation()



        //Navbar
        val medicationButonNavbar = findViewById<Button>(R.id.medicationButtonNavbar)
        medicationButonNavbar.setOnClickListener {
            val intent = Intent(this@MedicalRound, Medication::class.java)
            startActivity(intent)
        }

        val notesButtonNavbar = findViewById<Button>(R.id.notesButtonNavbar)
        notesButtonNavbar.setOnClickListener {
            val intent = Intent(this@MedicalRound, Notes::class.java)
            startActivity(intent)
        }

        val appointmentButtonNavbar = findViewById<Button>(R.id.appointmentButtonNavbar)
        appointmentButtonNavbar.setOnClickListener {
            val intent = Intent(this@MedicalRound, Appointments::class.java)
            startActivity(intent)
        }

        val diagnosticFindingsButtonNavbar = findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        diagnosticFindingsButtonNavbar.setOnClickListener {
            val intent = Intent(this@MedicalRound, DiagnosticFindings::class.java)
            startActivity(intent)
        }


    }

    private fun getMedicalRoundInformation()
    {
        dbref = FirebaseDatabase.getInstance().getReference("newMedicalRound/medicalRound")

        dbref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()){}
                for(medicalRoundSnapshot in snapshot.children){
                    val medicalRound = medicalRoundSnapshot.getValue(MedicalRoundData::class.java)
                    medicalRoundArrayList.add(medicalRound!!)
                }

               medicalRoundRecyclerView.adapter = MyAdapterMedicalRound(medicalRoundArrayList)
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

}
