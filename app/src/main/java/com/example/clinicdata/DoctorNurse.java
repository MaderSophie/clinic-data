package com.example.clinicdata;

public class DoctorNurse {
    Integer id;
    String firstname;
    String surname;
    String titel;
    String department;

    public DoctorNurse(Integer id, String firstname, String surname, String titel, String department) {
        this.id = id;
        this.firstname = firstname;
        this.surname = surname;
        this.titel = titel;
        this.department = department;
    }

    public DoctorNurse(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}
