package com.example.clinicdata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
//import com.example.clinicdata.database.DatabaseHelper

class PatientList: AppCompatActivity() {

    //private lateinit var dbHelper: DatabaseHelper
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.patientlist)

        //dbHelper = DatabaseHelper(this)
        recyclerView = findViewById(R.id.patientlistView)
        recyclerView.layoutManager = LinearLayoutManager(this)

        //val personList = dbHelper.getAllPersons()

       // val adapter = PatientAdapter(personList)
       // recyclerView.adapter = adapter
    }
}