package com.example.clinicdata.usermgnt;

public class ApplicationUser {
    Integer id;
    String password;
    String name;

    Roles role;

    public ApplicationUser(Integer id, String password, String name, Roles role) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }
}
