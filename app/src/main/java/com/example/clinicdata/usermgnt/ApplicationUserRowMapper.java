package com.example.clinicdata.usermgnt;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ApplicationUserRowMapper implements RowMapper<ApplicationUser> {
    @Override
    public ApplicationUser mapRow(ResultSet rs, int rowNum) throws SQLException {
        Roles role = new Roles(rs.getString(4), rs.getInt(5));
        return new ApplicationUser(rs.getInt(1), rs.getString(2), rs.getString(3), role);
    }
}
