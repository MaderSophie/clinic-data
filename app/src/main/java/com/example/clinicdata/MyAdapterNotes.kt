package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapterNotes(private val noteList : ArrayList<NotesData>) : RecyclerView.Adapter<MyAdapterNotes.MyViewHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.notes_item,
        parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentitem = noteList[position]

        holder.textNotes.text = currentitem.textNotes
    }

    class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        val textNotes : TextView = itemView.findViewById(R.id.databaseNote)
    }
}