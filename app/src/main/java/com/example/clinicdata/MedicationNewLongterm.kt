package com.example.clinicdata

import android.os.Bundle
import android.text.TextUtils
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.core.view.View
import org.attoparser.util.TextUtil

class MedicationNewLongterm: AppCompatActivity() {

    private lateinit var database: FirebaseDatabase
    private lateinit var myRef: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medication_new_longterm)

        val dateTextView = findViewById<TextView>(R.id.startTimeLongterm)
        val date = Date()
        val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

        dateTextView.text = format.format(date)

        val saveButton = findViewById<Button>(R.id.saveMedicationNewButton)
        val name = findViewById<EditText>(R.id.spinnerNewMedication)
        val prescriber = findViewById<EditText>(R.id.editPrescriberText)
        val dosis = findViewById<EditText>(R.id.EditDosisText)
        val frequency = findViewById<EditText>(R.id.editFrequencyText)
        val startTime = findViewById<EditText>(R.id.startTimeLongterm)
        val endTime = findViewById<EditText>(R.id.endTimeLongterm)


        /*val spinner = findViewById<Spinner>(R.id.spinnerNewMedication)
        val medicationTypes = resources.getStringArray(R.array.medication_types)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, medicationTypes)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter*/

        /*spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedMedicationType = parent.getItemAtPosition(position).toString()
                // Speichern Sie den ausgewählten Wert in einer Variablen
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Nichts ausgewählt
            }
        }*/

        saveButton.setOnClickListener{
            if (validateInputs(name, prescriber, dosis, frequency, startTime,endTime)) {
                saveMedicationData(
                    name.text.toString(),
                    prescriber.text.toString(),
                    dosis.text.toString(),
                    frequency.text.toString(),
                    startTime.text.toString(),
                    endTime.text.toString()


                )

                name.setText("")
                prescriber.setText("")
                dosis.setText("")
                frequency.setText("")
                startTime.setText("")
                endTime.setText("")
                finish()
            } else{
                Toast.makeText(this, "Bitte füllen Sie alle erforderlichen Felder aus", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validateInputs(
        name: EditText,
        prescriber: EditText,
        dosis: EditText,
        frequency: EditText,
        startTime: EditText,
        endTime: EditText
    ): Boolean{
        return !(TextUtils.isEmpty(name.text) &&
                TextUtils.isEmpty(prescriber.text) &&
                TextUtils.isEmpty(dosis.text) &&
                TextUtils.isEmpty(frequency.text) &&
                TextUtils.isEmpty(startTime.text) &&
                TextUtils.isEmpty(endTime.text))
    }

    private fun saveMedicationData(
        name: String,
        prescriber: String,
        dosis: String,
        frequency: String,
        startTime: String,
        endTime: String,
        //medicationTypes: String
    ){
        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("medicationNewLongterm")

        val medicationData = hashMapOf(
            "name" to name,
            "prescriber" to prescriber,
            "dosis" to dosis,
            "frequency" to frequency,
            "startTime" to startTime,
            "endTime" to endTime,
            //"medicationType" to medicationTypes

        )

        //val selectedMedicationType = spinner.getSelectedItem().toString()


        myRef.child("longtermMedications").push().setValue(medicationData)
            .addOnSuccessListener {
                Toast.makeText(this, "Medication data saved successfully", Toast.LENGTH_SHORT).show()
            }
    }
}
