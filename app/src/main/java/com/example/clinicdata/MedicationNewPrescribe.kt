package com.example.clinicdata

import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class MedicationNewPrescribe: AppCompatActivity() {

    private lateinit var database: FirebaseDatabase
    private lateinit var myRef: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.medication_new_prescribe)

        val dateTextView = findViewById<TextView>(R.id.startTimePrescribe)
        val date = Date()
        val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

        dateTextView.text = format.format(date)

        val name = findViewById<EditText>(R.id.spinnerNewMedication)
        val saveButtonPrescribe = findViewById<Button>(R.id.saveMedicationNewButton)
        val prescriberPrescribe = findViewById<EditText>(R.id.editPrescriberText)
        val dosisPrescribe = findViewById<EditText>(R.id.EditDosisText)
        val frequencyPrescribe = findViewById<EditText>(R.id.editFrequencyText)
        val startTimePrescribe = findViewById<EditText>(R.id.startTimePrescribe)
        val endTimePrescribe = findViewById<EditText>(R.id.endTimePrescribe)

        saveButtonPrescribe.setOnClickListener{
            if (validateInputs(name, prescriberPrescribe, dosisPrescribe, frequencyPrescribe, startTimePrescribe,endTimePrescribe)) {
                saveMedicationData(
                    name.text.toString(),
                    prescriberPrescribe.text.toString(),
                    dosisPrescribe.text.toString(),
                    frequencyPrescribe.text.toString(),
                    startTimePrescribe.text.toString(),
                    endTimePrescribe.text.toString()
                )

                name.setText("")
                prescriberPrescribe.setText("")
                dosisPrescribe.setText("")
                frequencyPrescribe.setText("")
                startTimePrescribe.setText("")
                endTimePrescribe.setText("")
                finish()
            } else{
                Toast.makeText(this, "Bitte füllen Sie alle erforderlichen Felder aus", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validateInputs(
        name: EditText,
        prescriberPrescribe: EditText,
        dosisPrescribe: EditText,
        frequencyPrescribe: EditText,
        startTimePrescribe: EditText,
        endTimePrescribe: EditText
    ): Boolean{
        return !(TextUtils.isEmpty(name.text) &&
                TextUtils.isEmpty(prescriberPrescribe.text) &&
                TextUtils.isEmpty(dosisPrescribe.text) &&
                TextUtils.isEmpty(frequencyPrescribe.text) &&
                TextUtils.isEmpty(startTimePrescribe.text) &&
                TextUtils.isEmpty(endTimePrescribe.text))
    }

    private fun saveMedicationData(
        name: String,
        prescriberPrescribe: String,
        dosisPrescribe: String,
        frequencyPrescribe: String,
        startTimePrescribe: String,
        endTimePrescribe: String
    ){
        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("medicationNewPrescribe")

        val medicationData = hashMapOf(
            "name" to name,
            "prescriberPrescribe" to prescriberPrescribe,
            "dosisPrescribe" to dosisPrescribe,
            "frequencyPrescribe" to frequencyPrescribe,
            "startTimePrescribe" to startTimePrescribe,
            "endTimePrescribe" to endTimePrescribe
        )

        myRef.child("prescribeMedications").push().setValue(medicationData)
            .addOnSuccessListener {
                Toast.makeText(this, "Medication data saved successfully", Toast.LENGTH_SHORT).show()
            }
    }
}
