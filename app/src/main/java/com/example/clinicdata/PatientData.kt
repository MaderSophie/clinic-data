package com.example.clinicdata

import android.annotation.SuppressLint
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.util.Date
import java.util.Locale

class PatientData : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.patientdata)

        val currentDateAppointments = findViewById<TextView>(R.id.currentDateAppointments)
        val currentDateMedicalRound = findViewById<TextView>(R.id.currentDateMedicalRound)

        // Aktuelles Datum setzen
        val dateFormatAppointments = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val dateAppointments = Date()
        val currentDateA: String = dateFormatAppointments.format(dateAppointments)
        currentDateAppointments.setText("Datum: $currentDateA")

        val dateFormatMedicalRound = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val dateMedicalRound = Date()
        val currentDateMR: String = dateFormatMedicalRound.format(dateMedicalRound)
        currentDateMedicalRound.setText("Datum: $currentDateMR")
    }
}