package com.example.clinicdata

import android.os.Bundle
import android.os.PersistableBundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class NewNotes: AppCompatActivity() {
   private lateinit var database: FirebaseDatabase
    private lateinit var  myRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notes_new)

       val saveButtonNotes = findViewById<Button>(R.id.saveNotes)
       val textNotes = findViewById<EditText>(R.id.editTextNotes)

        saveButtonNotes.setOnClickListener{
            if (validateInputs(textNotes)){
                saveNotesData(
                    textNotes.text.toString()
                )

                textNotes.setText("")
                finish()
            }else  {
                Toast.makeText(this, "Bitte füllen Sie alle erforderlichen Felder aus", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validateInputs(
        textNotes: EditText
    ): Boolean{
        return !(TextUtils.isEmpty(textNotes.text))
    }

    private fun saveNotesData(
        textNotes: String
    ){
        database = FirebaseDatabase.getInstance()
        myRef = database.getReference("Notes")

        val newNotes = hashMapOf(
            "textNotes" to textNotes
        )

        myRef.child("newNotes").push().setValue(newNotes)
            .addOnSuccessListener {
                Toast.makeText(this, "Note saved successfully", Toast.LENGTH_SHORT).show()
            }

    }
}
