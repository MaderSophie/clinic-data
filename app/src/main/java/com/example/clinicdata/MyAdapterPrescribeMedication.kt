package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapterPrescribeMedication(private val prescribeMedicationList : ArrayList<PrescribeMedicationData>) : RecyclerView.Adapter<MyAdapterPrescribeMedication.MyViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.medication_prescribe_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyAdapterPrescribeMedication.MyViewHolder, position: Int) {
        val currentitem = prescribeMedicationList[position]

        holder.name.text = currentitem.name
        holder.dosisPrescribe.text = currentitem.dosisPrescribe
        holder.endTimePrescribe.text = currentitem.endTimePrescribe
        holder.frequencyPrescribe.text = currentitem.frequencyPrescribe
        holder.prescriberPrescribe.text = currentitem.prescriberPrescribe
        holder.startTimePrescribe.text = currentitem.startTimePrescribe
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val name : TextView = itemView.findViewById(R.id.databaseMedicationPrescribe)
        val dosisPrescribe : TextView = itemView.findViewById(R.id.databaseDosisPrescribe)
        val endTimePrescribe : TextView = itemView.findViewById(R.id.databaseEndTimePrescribe)
        val frequencyPrescribe : TextView = itemView.findViewById(R.id.databaseFrequencyPrescribe)
        val prescriberPrescribe : TextView = itemView.findViewById(R.id.databasePrescriberPrescribe)
        val startTimePrescribe : TextView = itemView.findViewById(R.id.databaseStartTimePrescribe)
    }

    override fun getItemCount(): Int {
        return prescribeMedicationList.size
    }

}