package com.example.clinicdata.usermgnt;

public interface UserDao {
    ApplicationUser findUserByName(String name);
}
