package com.example.clinicdata

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.activity.ComponentActivity
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.clinicdata.ui.theme.ClinicDataTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.startpage)

        val newPatientButton = findViewById<Button>(R.id.addPatientButton)
        val showPatientList = findViewById<Button>(R.id.patientListButton)

        newPatientButton.setOnClickListener {
            val intent = Intent(this@MainActivity, NewPatient::class.java)

            startActivity(intent)
        }

        showPatientList.setOnClickListener {
            val intent = Intent(this@MainActivity, PatientList::class.java)

            startActivity(intent)
        }


        val buttonGroup = findViewById<LinearLayout>(R.id.buttonGroup)

        val medicationButtonNavbar = buttonGroup.findViewById<Button>(R.id.medicationButtonNavbar)
        val notesButtonNavbar = buttonGroup.findViewById<Button>(R.id.notesButtonNavbar)
        val appointmentButtonNavbar = buttonGroup.findViewById<Button>(R.id.appointmentButtonNavbar)
        val diagnosticFindingsButtonNavbar = buttonGroup.findViewById<Button>(R.id.diagnosticFindingsButtonNavbar)
        val medicalRoundButtonNavbar = buttonGroup.findViewById<Button>(R.id.medicalRoundButtonNavbar)

        medicationButtonNavbar.setOnClickListener(this::startMenuItemClicked)
        notesButtonNavbar.setOnClickListener(this::startMenuItemClicked)
        appointmentButtonNavbar.setOnClickListener(this::startMenuItemClicked)
        diagnosticFindingsButtonNavbar.setOnClickListener(this::startMenuItemClicked)
        medicalRoundButtonNavbar.setOnClickListener(this::startMenuItemClicked)
    }


    fun startMenuItemClicked(view: View) {
        when (view.id) {
            R.id.medicationButtonNavbar -> {
                // Your code to handle the click event for the medication button
                val intent = Intent(this, Medication::class.java)
                startActivity(intent)
            }
            R.id.notesButtonNavbar -> {
                // Your code to handle the click event for the notes button
                val intent = Intent(this, Notes::class.java)
                startActivity(intent)
            }
            R.id.appointmentButtonNavbar -> {
                val intent = Intent(this, Appointments::class.java)
                startActivity(intent)
            }
            R.id.diagnosticFindingsButtonNavbar -> {
                val intent = Intent(this, DiagnosticFindings::class.java)
                startActivity(intent)
            }
            R.id.medicalRoundButtonNavbar -> {
                val intent = Intent(this, MedicalRound::class.java)
                startActivity(intent)
            }
        }

    }

}

