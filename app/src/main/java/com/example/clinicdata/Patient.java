package com.example.clinicdata;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public class Patient {
Integer id;
String firstname;
String surname;
String gender;
Date birthdate;
String insurance_status;
String insurance_name;
Integer insurance_number;
String diagnosis;
String address;
String therapeutic_measures;
String phone_number;
Integer plz;

    public Patient(Integer id, String firstname, String surname, String gender, Date birthdate, String insurance_status, String insurance_name, Integer insurance_number, String diagnosis, String address, String therapeutic_measures, String phone_number, Integer plz) {
        this.id = id;
        this.firstname = firstname;
        this.surname = surname;
        this.gender = gender;
        this.birthdate = birthdate;
        this.insurance_status = insurance_status;
        this.insurance_name = insurance_name;
        this.insurance_number = insurance_number;
        this.diagnosis = diagnosis;
        this.address = address;
        this.therapeutic_measures = therapeutic_measures;
        this.phone_number = phone_number;
        this.plz = plz;
    }

    public Patient(){

    }

    public Patient(@Nullable String id, @Nullable String firstName, @Nullable String surname, @Nullable String birthdate) {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getInsurance_status() {
        return insurance_status;
    }

    public void setInsurance_status(String insurance_status) {
        this.insurance_status = insurance_status;
    }

    public String getInsurance_name() {
        return insurance_name;
    }

    public void setInsurance_name(String insurance_name) {
        this.insurance_name = insurance_name;
    }

    public Integer getInsurance_number() {
        return insurance_number;
    }

    public void setInsurance_number(Integer insurance_number) {
        this.insurance_number = insurance_number;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTherapeutic_measures() {
        return therapeutic_measures;
    }

    public void setTherapeutic_measures(String therapeutic_measures) {
        this.therapeutic_measures = therapeutic_measures;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public Integer getPlz() {
        return plz;
    }

    public void setPlz(Integer plz) {
        this.plz = plz;
    }
}