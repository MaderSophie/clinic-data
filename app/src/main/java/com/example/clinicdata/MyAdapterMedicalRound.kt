package com.example.clinicdata

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapterMedicalRound(private val medicalRoundList : ArrayList<MedicalRoundData>) : RecyclerView.Adapter<MyAdapterMedicalRound.MyViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.medical_round_item,parent,false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyAdapterMedicalRound.MyViewHolder, position: Int) {
        val currentitem = medicalRoundList[position]

        holder.doctor.text = currentitem.doctor
        holder.medicalRound.text = currentitem.medicalRound
        holder.symptoms.text = currentitem.symptoms
        holder.anamnesis.text = currentitem.anamnesis
    }

    class MyViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView)
    {
        val doctor : TextView = itemView.findViewById(R.id.databaseDoctor)
        val medicalRound : TextView = itemView.findViewById(R.id.databaseMedicalRound)
        val symptoms : TextView = itemView.findViewById(R.id.databaseSymptoms)
        val anamnesis : TextView = itemView.findViewById(R.id.databaseAnamnesis)
    }

    override fun getItemCount(): Int {
       return medicalRoundList.size
    }

}