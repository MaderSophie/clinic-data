/*package com.example.clinicdata.dataaccess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryMedicationDao implements MedicationDao{

    private Map<Integer, Medication> database;

    public InMemoryMedicationDao() {
        this.database = new HashMap<>();
    }
    @Override
    public List<Medication> loadAllMedications() {
        return new ArrayList<>(this.database.values());
    }

    @Override
    public Medication loadMedication(Integer id) {
        return this.database.get(id);
    }

    @Override
    public void save(Medication m) {
        this.database.put(m.getId(), m);
    }

    @Override
    public void delete(Integer id) {
        this.database.remove(id);
    }
}*/
